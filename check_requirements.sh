#!/usr/bin/env bash

#constants
readonly jdkVersion="11"
readonly mvnVersion="3.6"
readonly gitVersion="2"
readonly dockerVersion="19.03.1" # corresponds to Docker Desktop Community 2.1.0.1 allow older?
readonly dockerCommand="docker run --name=mysql -p 127.0.0.1:3306:3306 -e MYSQL_ROOT_PASSWORD=start01 -e MYSQL_DATABASE=KLEINANZEIGEN -d mysql:8.0.22"
readonly dockerPullCommand="docker pull mysql:8.0.22"
readonly dockerSearchCommand="docker ps -a --format '{{.Names}}'"
readonly RED='\033[0;31m'
readonly YEL='\033[1;33m'
readonly NC='\033[0m' # No Color
###################################################################


#pass requirement string as first, and provided as second argument.
compareVersionArray() {
  #create version array
  IFS='.' read -ra requiredVersionArray <<< "${1}"
  IFS='.' read -ra installedVersionArray <<< "${2}"

  for i in "${!requiredVersionArray[@]}"; do
    if [[ "${requiredVersionArray[$i]}" > ${installedVersionArray[$i]} ]] ; then #compare version numbers
      return 1 #return error if requirement is not met
    fi
  done
  return 0
}


#check java version
installedVersion=$(java -version 2>&1 | awk -F'["]' '/version/ {print $2}')
if compareVersionArray "${jdkVersion}" "${installedVersion}" ; then
  printf "java version ok.\n"
else
  printf "${RED}java version ${installedVersion} not ok!${NC}\n"
fi

#check mvn Version
installedVersion=$(mvn -v 2>&1 | awk -F'[ ]' '/Apache Maven / {print $3}')
if compareVersionArray "${mvnVersion}" "${installedVersion}" ; then
  printf "maven version ok.\n"
else
  printf "${RED}maven version ${installedVersion} not ok!${NC}\n"
fi


#check git Version
installedVersion=$( git --version 2>&1 | awk -F'[ ]' '/git version / {print $3}')
if compareVersionArray "${gitVersion}" "${installedVersion}" ; then
  printf "git version ok.\n"
else
  printf "${RED}git version ${installedVersion} not ok!${NC}\n"
fi

#check dockerVersion Version
installedVersion=$( docker --version 2>&1 | awk -F'[ ,-]' '/Docker version / {print $3}')
if compareVersionArray "${dockerVersion}" "${installedVersion}" ; then
  printf "docker version ok.\n"
else
  printf "${RED}docker version ${installedVersion} not ok!${NC}\n"
fi

#check docker pull
if [ "$(id -u)" != 0 ] ; then
  printf "${YEL}you propably need root to use docker. (at least if your system is secure.)${NC}\n"
fi

if eval ${dockerPullCommand} ; then
  printf "Mysql docker pulled.\n"
else
  printf "${RED}Mysql docker pull failed!${NC}\n"
fi

if [ $(eval ${dockerSearchCommand}) == mysql ] ; then
  if eval ${dockerStartCommand} ; then
    printf "Docker image started.\n"
  else
    printf "${RED}Couldn't start docker image!${NC}\n"
  fi
elif eval ${dockerCommand} ; then
  printf "Mysql docker started.\n"
else
  printf "${RED}Mysql docker start failed!${NC}\n"
fi
