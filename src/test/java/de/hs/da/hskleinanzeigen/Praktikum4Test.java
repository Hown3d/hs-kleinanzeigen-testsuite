package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

import static de.hs.da.hskleinanzeigen.TestUtil.BASE_PATH_USER;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum4Test {



    private static final String NOTEPAD_AD_ID = "1";

    private static final String NOTEPAD_PAYLOAD = "{\n" +
            "        \"advertisement\": {\n" +
            "            \"id\": " + NOTEPAD_AD_ID + ",\n" +
            "            \"type\": \"Offer\",\n" +
            "            \"category\": 1,\n" +
            "            \"title\": \"Titel\",\n" +
            "            \"description\": \"Beschreibung\",\n" +
            "            \"price\": 42,\n" +
            "            \"location\": \"Standort\",\n" +
            "            \"user\": 1\n" +
            "        },\n" +
            "        \"note\": \"Zimmer direkt bei der HS\"\n" +
            "    }\n";

    private static final String NOTEPAD_PAYLOAD_NOTE_CHANGED = "{\n" +
            "        \"advertisement\": {\n" +
            "            \"id\": " + NOTEPAD_AD_ID + ",\n" +
            "            \"type\": \"Offer\",\n" +
            "            \"category\": 1,\n" +
            "            \"title\": \"Titel\",\n" +
            "            \"description\": \"Beschreibung\",\n" +
            "            \"price\": 42,\n" +
            "            \"location\": \"Standort\",\n" +
            "            \"user\": 1\n" +
            "        },\n" +
            "        \"note\": \"Beste WG ever!\"\n" +
            "    }\n";

    private static final String NOTEPAD_PAYLOAD_INCOMPLETE = "{\n" +
            "   \"note\":\"Zimmer direkt bei der HS\"\n" +
            "}\n";

    private static final Set<String> expectedTables = new HashSet<>(Arrays.asList("AD", "CATEGORY", "USER", "NOTEPAD"));

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    private boolean initialized = false;

    @BeforeEach
    void setUp() throws Exception {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }

        prepareData();
    }

    private void prepareData() throws SQLException {
        if (!initialized) {
            TestUtil.insertData(connection, expectedTables);
            initialized = true;
        }
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Test
    void task5_checkSwaggerUIAvailable() {
        given().when().get("/hs-kleinanzeigen/swagger-ui/index.html")//
                .then().statusCode(HttpStatus.OK.value());
    }

    /** @noinspection unchecked*/
    @Test
    void task5_checkApiDocsAdvertisementsEndpoint() {
        Map<String, Map<String, Map<String, Object>>> paths = readApiDocsEndpoints();

        String path = "/hs-kleinanzeigen/api/advertisements/{id}";
        assertThat(paths).containsKey(path);
        assertThat(paths.get(path)).containsKey("get");
        assertThat(((List)paths.get(path).get("get").get("parameters"))).hasSize(1);
        assertThat(((Map)paths.get(path).get("get").get("responses"))).hasSizeGreaterThanOrEqualTo(2);
        assertThat(((Map)paths.get(path).get("get").get("responses"))).containsKeys("200", "404");
    }

    /** @noinspection unchecked*/
    @Test
    void task5_checkApiDocsUserEndpoint() {
        Map<String, Map<String, Map<String, Object>>> paths = readApiDocsEndpoints();

        String path = "/hs-kleinanzeigen/api/users";
        assertThat(paths).containsKey(path);
        assertThat(paths.get(path)).containsKey("get");
        assertThat(((List)paths.get(path).get("get").get("parameters"))).hasSize(2);
        assertThat(((Map)paths.get(path).get("get").get("responses"))).hasSizeGreaterThanOrEqualTo(2);
        assertThat(((Map)paths.get(path).get("get").get("responses"))).containsKeys("200", "204", "400");
    }

    /** @noinspection unchecked*/
    @Test
    void task5_checkApiDocsCategorieEndpoint() {
        Map<String, Map<String, Map<String, Object>>> paths = readApiDocsEndpoints();

        String path = "/hs-kleinanzeigen/api/categories";
        assertThat(paths).containsKey(path);
        assertThat(paths.get(path)).containsKey("post");
        assertThat(((List)paths.get(path).get("post").get("parameters"))).isNullOrEmpty();
        assertThat(((Map)paths.get(path).get("post").get("responses"))).hasSizeGreaterThanOrEqualTo(3);
        assertThat(((Map)paths.get(path).get("post").get("responses"))).containsKeys("201", "400", "409");
    }

    private Map<String, Map<String, Map<String, Object>>> readApiDocsEndpoints() {
        // Map<PATH, Map<OPERATION, Map<[tags|summary|operationId|parameters|responses], Object>>>
        return given().when().get("/hs-kleinanzeigen/v3/api-docs")//
                .then().statusCode(HttpStatus.OK.value())
                .extract().jsonPath().getMap("paths");
    }

    /**
     * checks if the endpoint for creating a user after adding the checks for email and name validation still returns http.201 (created)
     * and the returned user doesn't contain the password.
     */
    @Test
    void task6_createUserStatus201() {
        User user = new User();

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("$", hasKey("id"))
                .body("$", hasKey("email"))
                .body("$", hasKey("firstName"))
                .body("$", hasKey("lastName"))
                .body("$", hasKey("phone"))
                .body("$", hasKey("location"))
                .body("$", not(hasKey("password")));
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserBadEmailStatus400() {
        User user = new User();
        user.email = "invalidEmail";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserNoEmailStatus400() {
        User user = new User();
        user.email = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserPasswordToShortStatus400() {
        User user = new User();
        user.password = "kurz";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserNoPasswordStatus400() {
        User user = new User();
        user.password = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserNoFirstNameStatus400() {
        User user = new User();
        user.firstName = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserFirstNameTooLongStatus400() {
        User user = new User();
        user.firstName = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. " +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. " +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserNoLastNameStatus400() {
        User user = new User();
        user.lastName = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserLastNameTooLongStatus400() {
        User user = new User();
        user.lastName = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. " +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. " +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    void task7_liquibaseTablesExists() throws SQLException {
        Set<String> existingTables = new HashSet<>();
        DatabaseMetaData md = connection.getMetaData();
        try (ResultSet rs = md.getTables(null, null, "%", null)) {
            while (rs.next()) {
                existingTables.add(rs.getString("TABLE_NAME").toUpperCase());
            }
        }

        assertThat(existingTables).contains("DATABASECHANGELOG").withFailMessage("Liquibase meta data table 'DATABASECHANGELOG' not found in database");
        assertThat(existingTables).contains("DATABASECHANGELOGLOCK").withFailMessage("Liquibase meta data table 'DATABASECHANGELOGLOCK' not found in database");

        assertThat(existingTables).contains("CATEGORY").withFailMessage("table 'CATEGORY' not found in database");
        assertThat(existingTables).contains("AD").withFailMessage("table 'AD' not found in database");
    }

    @Test
    void task8_checkNotepadTableExists() throws SQLException {
        Set<String> existingTables = new HashSet<>();
        DatabaseMetaData md = connection.getMetaData();
        try (ResultSet rs = md.getTables(null, null, "%", null)) {
            while (rs.next()) {
                String tableName = rs.getString("TABLE_NAME");
                if (expectedTables.contains(tableName)) {
                    existingTables.add(tableName.toUpperCase());
                }
            }
        }

        Assertions.assertEquals(expectedTables.size(), existingTables.size());
    }

    /**
     * expected table layout:
     * ID	Integer, Primary Key, auto increment
     * USER_ID	Integer, not null, Foreign Key auf USER.ID
     * AD_ID	Integer, not null, Foreign Key auf AD.ID
     * NOTE	Varchar
     * CREATED	Timestamp, not null
     */
    @Test
    void task8_checkNotepadColumnsExist() throws SQLException {
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "ID", TestUtil.FieldType.INTEGER), "Column ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "USER_ID", TestUtil.FieldType.INTEGER), "Column USER_ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "AD_ID", TestUtil.FieldType.INTEGER), "Column AD_ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "NOTE", TestUtil.FieldType.VARCHAR), "Column NOTE is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "CREATED", TestUtil.FieldType.TIMESTAMP), "Column CREATED is missing or has invalid data type");
    }

    /**
     * checks if http get on the notepad of a user returns http.200 (OK)
     */
    @Test
    void task9_getNotepadStatus200() {
        given().log().all()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .pathParam("userId", 1)
                .accept("application/json")
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("size()", greaterThanOrEqualTo(1))
                .body("[0].id", notNullValue())
                .body("[0].advertisement", notNullValue())
                .body("[0].advertisement.id", allOf(instanceOf(Integer.class), notNullValue()))
                .body("[0].advertisement.type", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.title", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.price", allOf(instanceOf(Integer.class), notNullValue()))
                .body("[0].advertisement.location", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].note", allOf(instanceOf(String.class), notNullValue()));
    }

    /**
     * checks if requesting the notepad of a not existing user returns http.04 (Not found)
     */
    @Test
    void task9_getNotepadStatus404() {
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .pathParam("userId", -1)
                .accept("application/json")
                .when().get()
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * checks if creating a notepad returns http.201 (created)
     * and the notepad is persisted in the database
     */
    @Test
    void task10_createNotepadStatus200() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            final String deleteNotepadStatement = "DELETE FROM NOTEPAD\n" +
                    "WHERE AD_ID = " + NOTEPAD_AD_ID;
            statement.executeUpdate(deleteNotepadStatement);
        }

        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 1)
                .body(NOTEPAD_PAYLOAD)
                .accept("application/json")
                .when().put()
                .then().statusCode(HttpStatus.OK.value())
                .body("id", notNullValue())
                .body("note", equalTo("Zimmer direkt bei der HS"));

        try (Statement statement = connection.createStatement()) {
            final String query = "select * from NOTEPAD\n" +
                    "where AD_ID = \"" + NOTEPAD_AD_ID + "\" and USER_ID = 1";
            ResultSet result = statement.executeQuery(query);
            Assertions.assertTrue(result.next(), "No notepad found in Database for the User ID / 1 and Notepad ID / " + NOTEPAD_AD_ID + ".");
        }
    }

    /**
     * checks if posting an invalid notepad returns http.400 (Bad REQUEST)
     */
    @Test
    void task10_createNotepadStatus400() {
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 1)
                .body(NOTEPAD_PAYLOAD_INCOMPLETE)
                .accept("application/json")
                .when().put()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if updating a notepad returns HTTP 200
     */
    @Test
    void task10_updateNotepad200() {
        // add (or update) notepad for the first time
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 1)
                .body(NOTEPAD_PAYLOAD)
                .accept("application/json")
                .when().put().then().statusCode(HttpStatus.OK.value());

        // add notepad for the second time
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 1)
                .body(NOTEPAD_PAYLOAD_NOTE_CHANGED)
                .accept("application/json")
                .when().put()
                .then().statusCode(HttpStatus.OK.value())
                .body("note", equalTo("Beste WG ever!"));
    }

    /**
     * checks if deleting a notepad returns http.204 (no content)
     * and the notepad no longer exists in database
     */
    @Test
    void task11_deleteNotepadStatus204() throws SQLException {
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParams("userId", 1, "adID", NOTEPAD_AD_ID)
                .when().delete("{adID}")
                .then().statusCode(HttpStatus.NO_CONTENT.value());

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(ID) AS ANZAHL FROM NOTEPAD WHERE USER_ID = 1 AND AD_ID = " + NOTEPAD_AD_ID);
            resultSet.next();
            int numberOfNotepads = resultSet.getInt("ANZAHL");
            Assertions.assertEquals(0, numberOfNotepads, "The notepad for user ID /1 and AD ID /" + NOTEPAD_AD_ID + " still exists.");
        }
    }

    class User {
        String email = UUID.randomUUID().toString() + "@mynewdomain.de";
        String password = "secret";
        String firstName = "my first name";
        String lastName = "my second name";
        String phone = "0176822222222";
        String location = "Darmstadt";

        String toJson() {
            String json = "{\n";
            if (email != null) {
                json += "    \"email\": \"" + email + "\",\n";

            }
            if (password != null) {
                json += "    \"password\": \"" + password + "\",\n";

            }

            if (firstName != null) {


                json += "    \"firstName\": \"" + firstName + "\",\n";
            }

            if (lastName != null) {
                json += "    \"lastName\": \"" + lastName + "\",\n";

            }

            json += "    \"phone\": \"" + phone + "\",\n" +
                    "    \"location\": \"" + location + "\"\n" +
                    "}";
            return json;
        }
    }
}
