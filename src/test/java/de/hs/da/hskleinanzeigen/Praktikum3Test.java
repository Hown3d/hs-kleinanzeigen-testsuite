package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static de.hs.da.hskleinanzeigen.TestUtil.BASE_PATH_AD;
import static de.hs.da.hskleinanzeigen.TestUtil.BASE_PATH_USER;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;


@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum3Test {

    private final ArrayList<String> expectedTables = new ArrayList<>() {
        {
            add("AD");
            add("CATEGORY");
            add("USER");
        }
    };

    private static final String CATEGORY_NAME = "Nachhilfe";

    private static final int CATEGORY_ID = 1;

    private static final String CATEGORY_PAYLOAD = "{\n" +
            "   \"name\":\"" + CATEGORY_NAME + "\"\n" +
            "}\n";

    private static final String CATEGORY_PAYLOAD_INCOMPLETE = "{\n" +
            "}\n";

    private static final String CATEGORY_PAYLOAD_PARENT_NOT_FOUND = "{\n" +
            "   \"parentId\": 4711,\n" +
            "}\n";

    private static final int pageStart = 0;
    private static final int pageSize = 5;

    private static String typeNameValid = "Offer";

    private static final String USER_EMAIL = "studi@hs-da.de";

    private static final String CREATE_USER_PAYLOAD = "{\n" +
            "   \"email\":\"" + USER_EMAIL + "\",\n" +
            "   \"password\":\"secret\",\n" +
            "   \"firstName\":\"Thomas\",\n" +
            "   \"lastName\":\"Müller\",\n" +
            "   \"phone\":\"069-123456\",\n" +
            "   \"location\":\"Darmstadt\"\n" +
            "}\n";

    private static final String CREATE_USER_PAYLOAD_INCOMPLETE = "{\n" +
            "   \"password\":\"secret\",\n" +
            "   \"firstName\":\"Thomas\",\n" +
            "   \"lastName\":\"Müller\",\n" +
            "   \"phone\":\"069-123456\",\n" +
            "   \"location\":\"Darmstadt\"\n" +
            "}\n";

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @BeforeEach
    void setUp() throws Exception {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }

        TestUtil.insertData(connection, expectedTables);
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }


    /**
     * checks if creating a category works
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task1_createCategoryStatus200() throws SQLException {
        TestUtil.deleteCategoryInDatabase(CATEGORY_NAME, connection);

        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(CATEGORY_PAYLOAD)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("id", notNullValue())
                .body("name", equalTo(CATEGORY_NAME));

        final String query = "select count(*) from CATEGORY where NAME = \"" + CATEGORY_NAME + "\"";
        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(query);
            Assertions.assertTrue(result.next());
            Assertions.assertEquals(1, result.getInt(1));
        }
    }

    /**
     * checks if creating an invalid category returns Http.400 (Bad Request)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task1_createCategoryStatus400() throws SQLException {
        TestUtil.deleteCategoryInDatabase(CATEGORY_NAME, connection);

        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(CATEGORY_PAYLOAD_PARENT_NOT_FOUND)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }


    /**
     * checks if creating a category twice returns Http.409 (Conflict)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task1_createCategoryStatus409() throws SQLException {
        TestUtil.deleteCategoryInDatabase(CATEGORY_NAME,connection);

        // add category for the first time
        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(CATEGORY_PAYLOAD)
                .accept("application/json")
                .when().post().then().statusCode(HttpStatus.CREATED.value());

        // add category for the second time
        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(CATEGORY_PAYLOAD)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CONFLICT.value());
    }

    @Test
    void task2_getAdPageStatus200() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("pageStart", pageStart, "pageSize", pageSize)
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("content", notNullValue())
                .body("pageable", notNullValue())
                .body("totalPages", notNullValue())
                .body("totalElements", notNullValue());

    }

    /**
     * checks if requesting an invalid User page returns a page and http.400 (Bad request)
     */
    @Test
    void task2_getAdPageInvalidPageSizeStatus400() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("pageStart", 0, "pageSize", -1)
                .when().get()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if requesting an invalid User page returns a page and http.400 (Bad request)
     */
    @Test
    void task2_getAdPageInvalidPageStartStatus400() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("pageStart", -1, "pageSize", 0)
                .when().get()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if requesting an invalid User page returns a page and http.400 (Bad request)
     */
    @Test
    void task2_getAdPageInvalidPageStartAndSizeStatus400() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("pageStart", -1, "pageSize", -1)
                .when().get()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());

    }

    @Test
    void task2_getAdPageStatus200WithAllParams() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("type", typeNameValid, "category", CATEGORY_ID, "priceFrom" , 0, "priceTo", 600,"pageStart", pageStart, "pageSize", pageSize)
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("content", notNullValue())
                .body("pageable", notNullValue())
                .body("totalPages", notNullValue())
                .body("totalElements", notNullValue());

    }

    @Test
    void task2_getAdPageStatus204() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("type", typeNameValid, "category", CATEGORY_ID, "priceFrom" , 100000, "priceTo", 100000,"pageStart", pageStart, "pageSize", pageSize)
                .when().get()
                .then().statusCode(HttpStatus.NO_CONTENT.value());

    }

    /**
     * checks if the expected tables exists
     * User table is new
     *
     * @throws SQLException if the connection failed or the database is badly configured
     */
    @Test
    void task3_checkUserColumnsExist() throws SQLException {
        boolean hasUser = false;
        List<String> existingTables = new ArrayList<>();

        DatabaseMetaData md = connection.getMetaData();
        ResultSet rs = md.getTables(null, null, "%", null);
        while (rs.next()) {
            if (expectedTables.contains(rs.getString("TABLE_NAME"))) {
                existingTables.add(rs.getString("TABLE_NAME"));
            }
        }

        Assertions.assertEquals(expectedTables.size(), existingTables.size());

        ResultSet columns = connection.getMetaData().getColumns(null, null, "AD", null);
        while (columns.next()) {
            if (columns.getString("COLUMN_NAME").equals("USER_ID")) {
                hasUser = true;
                break;
            }
        }
        Assertions.assertTrue(hasUser);
    }

    /**
     * expected table layout:
     * ID	Integer, Primary Key, auto increment
     * EMAIL	Varchar, not null
     * PASSWORD	Varchar, not null
     * FIRST_NAME	Varchar
     * LAST_NAME	Varchar
     * PHONE	Varchar
     * LOCATION	Varchar
     * CREATED	Timestamp, not null
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task3_newUserTableExists() throws SQLException {
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "ID", TestUtil.FieldType.INTEGER), "Column ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "EMAIL", TestUtil.FieldType.VARCHAR), "Column EMAIL is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "PASSWORD", TestUtil.FieldType.VARCHAR), "Column PASSWORD is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "FIRST_NAME", TestUtil.FieldType.VARCHAR), "Column FIRST_NAME is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "LAST_NAME", TestUtil.FieldType.VARCHAR), "Column LAST_NAME is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "PHONE", TestUtil.FieldType.VARCHAR), "Column PHONE is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "LOCATION", TestUtil.FieldType.VARCHAR), "Column LOCATION is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "CREATED", TestUtil.FieldType.TIMESTAMP), "Column CREATED is missing or has invalid data type");
    }

    /**
     * checks if inserting users works
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task4_insertUserStatus200() throws SQLException {
        TestUtil.deleteUserInDatabase(USER_EMAIL, connection);

        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(CREATE_USER_PAYLOAD)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("id", notNullValue())
                .body("email", equalTo(USER_EMAIL))
                .body("firstName", equalTo("Thomas"))
                .body("lastName", equalTo("Müller"))
                .body("phone", equalTo("069-123456"))
                .body("location", equalTo("Darmstadt"))
                .body("$", not(hasKey("password")));


        final String query = "select count(*) from USER where EMAIL = \"" + USER_EMAIL + "\"";
        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(query);
            Assertions.assertTrue(result.next());
            Assertions.assertEquals(1, result.getInt(1));
        }
    }

    /**
     * checks if inserting an invalid user returns Http.400 (Bad Request)
     *
     */
    @Test
    void task4_insertUserStatus400() {
        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(CREATE_USER_PAYLOAD_INCOMPLETE)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if inserting a user with the same email twice returns Http.409 (Conflict)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task4_insertUserStatus409() throws SQLException {
        // add user for the first time
        TestUtil.deleteUserInDatabase(USER_EMAIL, connection);
        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(CREATE_USER_PAYLOAD)
                .accept("application/json")
                .when().post().then().statusCode(HttpStatus.CREATED.value());

        // add user for the second time
        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(CREATE_USER_PAYLOAD)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CONFLICT.value());
    }


    /**
     * checks if requesting a User page returns a page and http.200 (OK)
     */
    @Test
    void task5_getUserPageStatus200() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .queryParams("pageStart", 0, "pageSize", 10)
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("content", notNullValue())
                .body("pageable", notNullValue())
                .body("totalPages", notNullValue())
                .body("totalElements", notNullValue());
    }

    /**
     * Tests the response if the requested page does not exist.
     * Be aware that this tests assumes that there are less than 100.000 users in database.
     */
    @Test
    void task5_getUserPageStatus204() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .queryParams("pageStart", 100000, "pageSize", 10)
                .when().get()
                .then().statusCode(HttpStatus.NO_CONTENT.value());
    }

    /**
     * checks if requesting an invalid User page returns a page and http.400 (Bad request)
     */
    @Test
    void task5_getUserPageInvalidPageSizeStatus400() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .queryParams("pageStart", 0, "pageSize", -1)
                .when().get()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if requesting an invalid User page returns a page and http.400 (Bad request)
     */
    @Test
    void task5_getUserPageInvalidPageStartStatus400() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .queryParams("pageStart", -1, "pageSize", 0)
                .when().get()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if requesting an invalid User page returns a page and http.400 (Bad request)
     */
    @Test
    void task5_getUserPageInvalidPageStartAndSizeStatus400() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .queryParams("pageStart", -1, "pageSize", -1)
                .when().get()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());

    }

    /**
     * checks if the requesting an ad matches the new specifications of task6
     */
    @Test
    void task6_getAdResult200() {
        given()
                .basePath(TestUtil.BASE_PATH_AD)
                .pathParam("id", 1)
                .accept("application/json")
                .when().get("{id}")
                .then().statusCode(HttpStatus.OK.value())
                .body("id", notNullValue())
                .body("type", equalTo("Offer"))
                .body("category", notNullValue())
                .body("user", notNullValue())
                .body("title", equalTo("Titel"))
                .body("description", equalTo("Beschreibung"))
                .body("price", equalTo(42))
                .body("location", equalTo("Standort"))
                .body("created", notNullValue());

    }

    /**
     * checks if requesting an illegal id returns Http.Bad Request
     */
    @Test
    void task6_getAdResult400() {
        given()
                .accept("application/json")
                .basePath(TestUtil.BASE_PATH_AD)
                .pathParam("id", -1)
                .when().get("{id}")
                .then().statusCode(HttpStatus.NOT_FOUND.value());

    }
}
